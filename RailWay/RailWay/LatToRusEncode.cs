﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RailWay
{
    static class LatToRusEncode
    {
        /// <summary>
        /// Статический класс и метод. Конвертирует входную строку кириллицу при условии, что есть латинские буквы
        /// аналогичные русским по стилю написания
        /// </summary>
        /// <param name="str">Входящая строка</param>
        /// <returns>Сконвертированная в кирриллицу строка</returns>
        public static string Converter(string str)
        {
            List<char> ch = str.ToList<char>();

            //Удаляет лишние пробелы
            int ind1 = 0;
            int ind2 = 0;
            for (int i = 0; i < ch.Count; i++)
            {
                ind1 = i; ind2 = ind1 + 1;
                try { if (ch[ind1] == ' ' && ' ' == ch[ind2]) ch.Remove(ch[ind1]); } 
                catch { continue; }
            }
            
            //Меняет символы латинницы в кириллицу
            for (int i = 0; i < ch.Count; i++ )
                switch (ch[i])
                {
                    case 'A': ch[i] = 'А'; break;
                    case 'E': ch[i] = 'Е'; break;
                    case 'T': ch[i] = 'Т'; break;
                    case 'O': ch[i] = 'О'; break;
                    case 'P': ch[i] = 'Р'; break;
                    case 'H': ch[i] = 'Н'; break;
                    case 'K': ch[i] = 'К'; break;
                    case 'X': ch[i] = 'Х'; break;
                    case 'C': ch[i] = 'С'; break;
                    case 'B': ch[i] = 'В'; break;
                    case 'M': ch[i] = 'М'; break;
                }

            string s = null;

            for (int i = 0; i < ch.Count; i++)
                s += ch[i].ToString();

            return s;
        }
    }
}
