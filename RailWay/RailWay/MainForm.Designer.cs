﻿namespace RailWay
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.log_tbx = new System.Windows.Forms.RichTextBox();
            this.start_btn = new System.Windows.Forms.Button();
            this.firstStation_tbx = new System.Windows.Forms.TextBox();
            this.secondStation_tbx = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.change_btn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // log_tbx
            // 
            this.log_tbx.Location = new System.Drawing.Point(29, 25);
            this.log_tbx.Name = "log_tbx";
            this.log_tbx.ReadOnly = true;
            this.log_tbx.Size = new System.Drawing.Size(421, 140);
            this.log_tbx.TabIndex = 4;
            this.log_tbx.Text = "";
            // 
            // start_btn
            // 
            this.start_btn.Location = new System.Drawing.Point(278, 72);
            this.start_btn.Name = "start_btn";
            this.start_btn.Size = new System.Drawing.Size(128, 26);
            this.start_btn.TabIndex = 3;
            this.start_btn.Text = "Поиск маршрута";
            this.start_btn.UseVisualStyleBackColor = true;
            this.start_btn.Click += new System.EventHandler(this.start_btn_Click);
            // 
            // firstStation_tbx
            // 
            this.firstStation_tbx.Location = new System.Drawing.Point(6, 35);
            this.firstStation_tbx.Name = "firstStation_tbx";
            this.firstStation_tbx.Size = new System.Drawing.Size(200, 20);
            this.firstStation_tbx.TabIndex = 1;
            // 
            // secondStation_tbx
            // 
            this.secondStation_tbx.Location = new System.Drawing.Point(6, 78);
            this.secondStation_tbx.Name = "secondStation_tbx";
            this.secondStation_tbx.Size = new System.Drawing.Size(200, 20);
            this.secondStation_tbx.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Введите название начальной станции:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Название конечной станции:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.change_btn);
            this.groupBox1.Controls.Add(this.start_btn);
            this.groupBox1.Controls.Add(this.firstStation_tbx);
            this.groupBox1.Controls.Add(this.secondStation_tbx);
            this.groupBox1.Location = new System.Drawing.Point(29, 180);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(421, 110);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // change_btn
            // 
            this.change_btn.Location = new System.Drawing.Point(278, 32);
            this.change_btn.Name = "change_btn";
            this.change_btn.Size = new System.Drawing.Size(128, 23);
            this.change_btn.TabIndex = 3;
            this.change_btn.Text = "Выбрать файлы";
            this.change_btn.UseVisualStyleBackColor = true;
            this.change_btn.Click += new System.EventHandler(this.change_btn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(304, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Выберите файлы:";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 304);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.log_tbx);
            this.Name = "MainForm";
            this.Text = "RailWay";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox log_tbx;
        private System.Windows.Forms.Button start_btn;
        private System.Windows.Forms.TextBox firstStation_tbx;
        private System.Windows.Forms.TextBox secondStation_tbx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button change_btn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}

