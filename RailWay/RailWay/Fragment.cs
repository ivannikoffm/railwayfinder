﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RailWay
{
    class Fragment
    {
        #region Fields & Properties
        private List<string> stationName;
        public List<string> StationName
        {
            get { return stationName; }
            set { stationName = value; }
        }

        private List<string> stationCode;
        public List<string> StationCode
        {
            get { return stationCode; }
            set { stationCode = value; }
        }

        private string list;
        public string List
        {
            get { return list; }
            set { list = value; }
        }

        private string station;
        public string Station
        {
            get { return station; }
            set { station = value; }
        }

        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private List<string> nodes;
        public List<string> Nodes
        {
            get { return nodes; }
            set { nodes = value; }
        }
        #endregion

        /// <summary>
        /// Конструктор Фрагмента
        /// </summary>
        /// <param name="num">ID Фрагмента</param>
        /// <param name="list">Идентификатор Листа, в котором находится фрагмент</param>
        public Fragment(int num, string list)
        { 
            this.id = num;
            this.list = list;
            stationName = new List<string>();
            stationCode = new List<string>();
            nodes = new List<string>();
        }

        #region Черновые поля
        //public bool isVisited;
        //public bool isDeadLock;
        //public bool isTransit;
        //public bool isGood;
        #endregion
    }
}
