﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace RailWay
{
    public partial class MainForm : Form
    {
        #region Fields
        string[] files;
        List<Fragment> Fragments;
        List<Fragment> map;
        List<Fragment> routeMap;
        List<int> route;
        Fragment startFragment;
        Fragment finishFragment;
        string station1;
        string station2;
        List<int[]> relations;
        List<int[]> routeStation;
        List<string> trainRoute;
        List<int[]> routes;
        List<int> routess;
        #endregion

        #region Main Methods

        public MainForm()
        {
            InitializeComponent();
            Fragments = new List<Fragment>();
            map = new List<Fragment>();
        }

        private void start_btn_Click(object sender, EventArgs e)
        {
            DoProgramm();
        }

        /// <summary>
        /// Основной метод. Реализует порядок выполнения других методов по шагам
        /// </summary>
        private void DoProgramm()
        {
            foreach (string f in openFileDialog.FileNames) //Парсинг XML файлов
                GetXmlData(f);

            if (firstStation_tbx.Text == "" || secondStation_tbx.Text == "") //Проверка ввода данных
            {
                log_tbx.Text += "Ошибка! Название станции пропущено!";
                return;
            }
            //Обработка стартовых данных (кодировка + приведение к кириллице)
            station1 = StringEncode(LatToRusEncode.Converter(firstStation_tbx.Text));
            station2 = StringEncode(LatToRusEncode.Converter(secondStation_tbx.Text));

            if (!CheckEnterStation(station1) || !CheckEnterStation(station2))//Проверка существования станций в загруженных листах
                return;

            //Поиск ID начального и конечного фрагментов
            startFragment = FindStationFragment(station1);
            finishFragment = FindStationFragment(station2);

            FindNodes(); //Поиск "узлов" - общих станций между фрагментами

            route = FindWay().ToList<int>(); //Поиск маршрута между стартом и финишом - основной алгоритм программы

            Output(); //Метод корректного вывода маршрута пользователю

            //Очистка памяти
            map.Clear();
            Fragments.Clear();
            route.Clear();
        }

        /// <summary>
        /// Ищет фрагмент, в котором расположена интересующая станция
        /// </summary>
        /// <param name="station">Станция, которую необходимо найти</param>
        /// <returns>Фрагмент, в котором расположена станция</returns>
        private Fragment FindStationFragment(string station)
        {
            string tstation = StringEncode(station);
            for (int i = 0; i < Fragments.Count; i++)
                for (int j = 0; j < Fragments[i].StationName.Count; j++)
                    if (Fragments[i].StationName[j] == tstation)
                        return Fragments[i];
            return null;
        }

        /// <summary>
        /// Ищет названия Узловых станций между Фрагментами
        /// </summary>
        private void FindNodes()
        {
            //Сверяет все названия станций между разными фрагментами, если есть совпадения, заносит станцию как "Узел"
            for (int i = 0; i < Fragments.Count; i++)
                for (int j = 0; j < Fragments[i].StationName.Count; j++)
                    for (int m = (i + 1); m < Fragments.Count; m++)
                        for (int n = 0; n < Fragments[m].StationName.Count; n++)
                            if (Fragments[i].StationName[j] == Fragments[m].StationName[n])
                            {
                                if (!Fragments[i].Nodes.Contains(Fragments[i].StationName[j]))
                                    Fragments[i].Nodes.Add(Fragments[i].StationName[j]);
                                if (!Fragments[m].Nodes.Contains(Fragments[m].StationName[n]))
                                    Fragments[m].Nodes.Add(Fragments[m].StationName[n]);
                                
                            }
            //Добавляет связанные фрагменты в некоторую логическую "карту"
            foreach (Fragment fr in Fragments)
                if (fr.Nodes.Count != 0)
                    map.Add(fr);
        }

        /// <summary>
        /// Формирует список "отношений" между фрагментами.
        /// </summary>
        private void GetRelations()
        { 
            relations = new List<int[]>(); //Динамический список массивов отношений.

            //Если видит совпадающий узел, то заносит два фрагмента в виде небольшого массива
            for (int i = 0; i < map.Count; i++)
                for (int j = 0; j < map[i].Nodes.Count; j++)
                    for (int m = i + 1; m < map.Count; m++)
                        for (int n = 0; n < map[m].Nodes.Count; n++)
                            if (map[m].Nodes[n] == map[i].Nodes[j])
                            {
                                relations.Add(new int[] { map[i].Id, map[m].Id, j });
                                relations.Add(new int[] { map[m].Id, map[i].Id, n });
                            }

            //Подчищает "отношения" в одном и том же фрагменте.
            for (int i = 0; i < relations.Count; i++)
                if (relations[i][0] == relations[i][1])
                    relations.Remove(relations[i]);
        }
        
        /// <summary>
        /// Основной алгоритм поиска маршрута
        /// </summary>
        /// <returns>Маршрут, в виде массива из ID фрагментов (перечень фрагментов от старта к финишу)</returns>
        private int[] FindWay()
        {
            GetRelations();

            int startIndex = 0;
            int finishIndex = 0;

            routes = new List<int[]>();

            //Заносим ID фрагментов старта и финиша во временные переменные
            startIndex = startFragment.Id; finishIndex = finishFragment.Id;

            //Первая запись в первом массиве тестового маршрута - ID фрагмента с начальной станцией
            routes.Add(new int[] { startIndex });

            //Далее, по списку связей программа ищет совпадающие фрагменты. Как только находит - добавляет новый маршрут
            //в новом массиве. Таким образом, получается список всех возможных маршрутов во все стороны от стартовой точки.
            //Программа будет перебирать все возможные варианты до тех пор, пока не найдет финишь.
            while (true)
            {
                for (int i = 0; i < relations.Count; i++)
                {
                    for (int j = 0; j < routes.Count; j++)
                    {
                        if (relations[i][0] == routes[j][routes[j].Length - 1])
                        {
                            int[] m = new int[routes[j].Length + 1];

                            for (int n = 0; n < routes[j].Length; n++)
                                m[n] = routes[j][n];
                            m[m.Length - 1] = relations[i][1];

                            if (relations[i][0] == finishIndex) { routes.Add(m); return routes[j]; }

                            bool b = false;
                            foreach (int[] routs in routes)
                                if (routs == m)
                                { b = true; break; }

                            if (!b) { routes.Add(m); break; }
                        }
                    }
                }
            }
        }

        #endregion

        #region Support Methods

        /// <summary>
        /// Преобразует все данные в единый формат кодировки
        /// </summary>
        /// <param name="str">Входной параметр(название станции)</param>
        /// <returns>Перекодированный результат</returns>
        private string StringEncode(string str)
        {
            //Преобразовывает строку в байтовый массив, перекодирует ее, а также переводит в верхний регистр
            byte[] b = Encoding.GetEncoding(0).GetBytes(str);
            string strToEnc = Encoding.Default.GetString(b);

            strToEnc = strToEnc.ToUpper();

            return strToEnc;
        }

        /// <summary>
        /// Проверяет присутствие введенной пользователем станции в загруженных фрагментах
        /// </summary>
        /// <param name="data">Введенная пользователем станция</param>
        /// <returns>Возвращает True, если введено станция есть в загруженных</returns>
        bool CheckEnterStation(string data)
        {
            for (int i = 0; i < Fragments.Count; i++)
                for (int j = 0; j < Fragments[i].StationName.Count; j++)
                    if (data == Fragments[i].StationName[j])
                    {
                        return true;
                    }
            log_tbx.Text += string.Format("\nНеверно введена станция {0}. Такой не существует в загруженных Листах.", data);
            return false;
        }

        /// <summary>
        /// Реализует вывод найденного маршрута на экран в заданном формате
        /// </summary>
        private void Output()
        {
            //Промежуточные временные переменные, необходимые для построения формата вывода
            routeMap = new List<Fragment>();
            List<string> myStatinons = new List<string>();

            //Создание последовательного списка фрагментов, по которым проходит маршрут
            for (int i = 0; i < route.Count; i++)
                for (int j = 0; j < map.Count; j++)
                    if (route[i] == map[j].Id)
                        routeMap.Add(map[j]);

            //Поиск и запоминание узлов в маршруте
            for (int i = 0; i < routeMap.Count; i++)
                for (int j = 0; j < routeMap[i].Nodes.Count; j++)
                    for (int m = i + 1; m < routeMap.Count; m++)
                        for (int n = 0; n < routeMap[m].Nodes.Count; n++)
                            if (routeMap[i].Nodes[j] == routeMap[m].Nodes[n])
                                routeMap[i].Station = routeMap[m].Nodes[n];

            //Чистка повторяющихся узлов в маршруте (так называемых "пересадок", которые могут присутствовать в множестве
            //Фрагментов, но при этом не являются реальными Фрагментами пути).
            for (int i = 0; i < routeMap.Count; i++)
                if (i > 0)
                    if (routeMap[i].Station == routeMap[i - 1].Station)
                        routeMap.Remove(routeMap[i]);


            log_tbx.Text += "\n----------------------------\n";
            log_tbx.Text += string.Format("{0} - {1}:\n", station1, station2);

            //Вывод в Лог станций и маршрутов в нужном формате
            for (int i = 0; i < routeMap.Count; i++)
            {
                if (i == 0)
                {
                    log_tbx.Text += string.Format("\n{0} - {1} по уч. {2} - {3} лист {4}", station1, routeMap[i].Station,
                        routeMap[i].StationName[0], routeMap[i].StationName[routeMap[i].StationName.Count - 1], routeMap[i].List);
                    continue;
                }

                if ((i + 1) == routeMap.Count)
                {
                    log_tbx.Text += string.Format("\n{0} - {1} по уч. {2} - {3} лист {4}", routeMap[i - 1].Station, station2,
                    routeMap[i].StationName[0], routeMap[i].StationName[routeMap[i].StationName.Count - 1], routeMap[i].List);
                    continue;
                }

                log_tbx.Text += string.Format("\n{0} - {1} по уч. {2} - {3} лист {4}", routeMap[i - 1].Station, routeMap[i].Station,
                            routeMap[i].StationName[0], routeMap[i].StationName[routeMap[i].StationName.Count - 1], routeMap[i].List);

            }
        }

        /// <summary>
        /// Вытягивает из XML-файла данные, необходимые для постоения Фрагментов пути
        /// </summary>
        /// <param name="file">XML-файл</param>
        private void GetXmlData(string file)
        {
            XmlTextReader reader = new XmlTextReader(file);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            string list = "";
            try
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "GDPSheet")
                            list = reader.GetAttribute("Name") + ".lgx";

                        if (reader.Name == "Fragment")
                        {
                            Fragment fragment = new Fragment(Fragments.Count, list);

                            while (reader.Read() && reader.Name != "Trains")
                            {
                                if (reader.Name == "RP" && reader.NodeType == XmlNodeType.Element)
                                {
                                    fragment.StationCode.Add(reader.GetAttribute("Code"));
                                    fragment.StationName.Add(LatToRusEncode.Converter(reader.GetAttribute("Name")));

                                }
                            }
                            Fragments.Add(fragment);
                        }
                    }
                }
            }
            catch { log_tbx.Text += "\nНе выбран ни один файл!\n"; return; }
        }

        /// <summary>
        /// Приводит к единой кодировке названия станций во фрагменте
        /// </summary>
        /// <param name="Fragments">Фрагмент</param>
        /// <returns>Фрагмент с перекодированными названиями станций</returns>
        List<Fragment> SetEncode(List<Fragment> Fragments)
        {
            List<Fragment> EncodeFragments = Fragments;
            for (int i = 0; i < EncodeFragments.Count; i++)
                for (int j = 0; j < EncodeFragments[i].StationName.Count; j++)
                    EncodeFragments[i].StationName[j] = StringEncode(EncodeFragments[i].StationName[j]);
            return EncodeFragments;
        }

        /// <summary>
        /// Элемент интерфейса для выбора необходимых фалов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void change_btn_Click(object sender, EventArgs e)
        {
            openFileDialog.InitialDirectory = ".";

            openFileDialog.Filter = "lgx files (*.lgx)|*.lgx";
            openFileDialog.FilterIndex = 1;
            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                files = openFileDialog.FileNames;
            }
        }

        #endregion


        #region Черновик
        /*

        private void GetRelations()
        {
            List<int[,]> rel = new List<int[,]>();

            for (int i = 0; i < map.Count; i++)
            {
                for (int j = 0; j < map[i].nodes.Count; j++)
                {
                    for (int m = i + 1; m < map.Count; m++)
                    {
                        for (int n = 0; n < map[m].nodes.Count; n++)
                        {
                            if (map[i].nodes[j] == map[m].nodes[n])
                            { 
                                rel.Add(new int[map[i].id, map[m].id]);
                            }
                        }
                    }
                }
            }

        }

        private void FindWay()
        {
            int startIndex = 0;
            int finishIndex = 0;
            routes = new List<int>();

            for (int i = 0; i < map.Count; i++ )
                if (map[i].num == start.num)
                    routes.Add(startIndex = i);

            for (int i = 0; i < map.Count; i++)
                if (map[i].num == finish.num)
                    finishIndex = i;

            while (startIndex != finishIndex)
            {
                for (int i = 0; i < map[startIndex].nodes.Count; i++)
                    for (int j = 0; j < map.Count; j++)
                        for (int k = 0; k < map[j].nodes.Count; k++)
                        {
                            if (map[j].nodes[k] == map[startIndex].nodes[i] && map[j].num != map[startIndex].num)//Есть связь
                            {
                                if (map[j] == map[finishIndex])
                                {
                                    routes.Add(map[j].num);
                                    return; }

                                //Проверка тупика и посещенного транзита
                                if ((map[startIndex].isDeadLock) || (map[startIndex].isTransit && map[startIndex].isVisited))
                                {
                                    if (map[j].isTransit && map[j].isVisited)
                                        {
                                            startIndex = map[j].num; //Делаем шаг
                                            routes.Add(map[j].num);
                                            j = map.Count; i = 0; break; //Сброс счетчиков
                                        }

                                    routes.RemoveAt(routes.Count - 1);
                                    startIndex = routes[routes.Count - 1];
                                    j = map.Count; i = 0; break; //Сброс счетчиков
                                }

                                if (map[j].nodes.Count <= 1)
                                    map[j].isDeadLock = true;

                                if (map[j].nodes.Count == 2)//Проверка транзита
                                    map[j].isTransit = true;

                                map[j].isVisited = true;

                                startIndex = map[j].num; //Делаем шаг
                                routes.Add(map[j].num);
                                j = map.Count; i = 0; break; //Сброс счетчиков
                            }
                        }
            }
        }
        
        
        int FindIndex(int id)
        {
            for (int i = 0; i < map.Count; i++)
                if (map[i].id == id)
                    id = i;
            return id;
        }

        private void GetPath()
        {
            int startIndex = 0;
            int finishIndex = 0;
            int tempIndex = 0;
            int prevIndex = 0;
            route = new List<int>();

            for (int i = 0; i < map.Count; i++)
                if (map[i].id == start.id)
                    route.Add(startIndex = i);
            tempIndex = startIndex;
            for (int i = 0; i < map.Count; i++)
                if (map[i].id == finish.id)
                    finishIndex = i;

            while (true)
            {
                for (int m = 0; m < map.Count; m++)
                {
                    for (int n = 0; n < map[m].nodes.Count; n++)
                    {
                        for (int i = 0; i < map[startIndex].nodes.Count; i++)
                        {
                            if (map[m].nodes[n] == map[startIndex].nodes[i])//Если есть связь
                            {
                                if (map[m] == map[finishIndex] || map[m].nodes[n] == station2)//Если находит финиш
                                {
                                    route.Add(map[m].id);
                                    return;
                                }

                                if (map[m].isDeadLock == true)
                                    continue;

                                if ((i - 1) == map[startIndex].nodes.Count)//Если переходить больше некуда - метка тупика
                                {
                                    map[startIndex].isDeadLock = true;
                                    continue;
                                }

                                if (map[m].nodes.Count == 1)//Блокируем тупик
                                {
                                    map[m].isDeadLock = true;
                                    continue;
                                }

                                if (map[m].id == prevIndex)
                                    continue;

                                for (int r = 0; r < route.Count; r++)
                                    if (route[r] == map[m].id)
                                        map[m].isVisited = true;

                                if (map[m].isVisited)
                                {
                                    map[startIndex].isDeadLock = true;
                                    continue;
                                }
                                    //try { continue; }
                                    //catch { if (startIndex == tempIndex) return; break; };

                                route.Add(startIndex);
                                prevIndex = map[startIndex].id;
                                startIndex = map[m].id;
                            }
                        }
                    }
                }
                //foreach (Fragment f in map)
                    //f.isVisited = false;
                startIndex = tempIndex;
                route.Clear();
            }
        }
        */
        #endregion



    }
}
